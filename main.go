package gocallapi

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

func This(api_url string, api_key string, body map[string]interface{}, print_response_body bool, request_type string, print_json_output bool) map[string]interface{} {
	marshalled_body, err := json.Marshal(body)
	if err != nil {
		fmt.Println("Something went wrong while marshalling the body, try again.")
		fmt.Println(err)
		os.Exit(1)
	}
	var http_new_request *http.Request
	if len(body) == 0 {
		http_new_request, err = http.NewRequest(request_type, api_url, nil)
		if err != nil {
			fmt.Println("Something went wrong while creating http request, try again.")
			fmt.Println(err)
			os.Exit(1)
		}
	} else {
		http_new_request, err = http.NewRequest(request_type, api_url, bytes.NewBuffer(marshalled_body))
		if err != nil {
			fmt.Println("Something went wrong while creating http request, try again.")
			fmt.Println(err)
			os.Exit(1)
		}
	}
	http_new_request.Header.Set("Authorization", api_key)
	http_client := &http.Client{}
	http_client_do_response, err := http_client.Do(http_new_request)
	if err != nil {
		fmt.Println("Something went wrong while sending http request, try again.")
		fmt.Println(err)
		os.Exit(1)
	}
	if http_client_do_response.StatusCode != 200 && !print_json_output {
		fmt.Printf("StatusCode : %d\n", http_client_do_response.StatusCode)
	}
	io_read_all_response, err := io.ReadAll(http_client_do_response.Body)
	if err != nil {
		fmt.Println("Something went wrong while reading http response, try again.")
		fmt.Println(err)
		os.Exit(1)
	}
	defer http_client_do_response.Body.Close()
	var api_response_body map[string]interface{}
	json.Unmarshal([]byte(io_read_all_response), &api_response_body)
	if http_client_do_response.StatusCode != 200 && print_json_output {
		api_response_body["statusCode"] = http_client_do_response.StatusCode
	}
	if !print_json_output {
		api_response_message, _ := api_response_body["message"].(string)
		if api_response_message == "Forbidden" {
			fmt.Println("Error: Invalid API key configured.")
			os.Exit(1)
		}
		if strings.HasPrefix(api_response_message, "error:") {
			fmt.Println(api_response_message)
			os.Exit(1)
		}
	}
	if print_response_body {
		fmt.Println(api_response_body)
	}
	return api_response_body
}
